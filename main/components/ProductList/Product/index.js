import React, { useMemo } from 'react'
import { View, Text, Image, Dimensions } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Product ({ style, data }) {
  const { img, label, category, price, sale } = data
  const { width } = Dimensions.get('window')
  const imageHeight = 365
  const imageWidth = 302

  const actualWidth = useMemo(() => {
    let _actualWidth, numberOfColumns, paddings
    if (width < 768) {
      paddings = 32
      numberOfColumns = 1
      _actualWidth = (width - paddings) / numberOfColumns
    } else if (width >= 768 && width < 1024) {
      paddings = 88
      numberOfColumns = 2
      _actualWidth = (width - paddings) / numberOfColumns
    } else if (width >= 1024 && width < 1280) {
      paddings = 64
      numberOfColumns = 2
      _actualWidth = (width * 0.7) / numberOfColumns - paddings
    } else {
      paddings = 60
      numberOfColumns = 4
      _actualWidth = (1120 * 0.75 - paddings) / numberOfColumns
    }

    return _actualWidth
  }, [width])

  let imageStyle = {
    width: actualWidth,
    height: actualWidth * imageHeight / imageWidth
  }

  return pug`
    View.root(style=style)
      Image(
        source={uri:img}
        style=imageStyle
        resizeMode='contain'
      )
      if sale
        View.sale
          Text.saleText sale
      View.row
        Text.label= label
        Text.label(styleName=['price'])= price
      Text.category= category
  `
})
