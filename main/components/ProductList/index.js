import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import products from './products.json'
import Product from './Product'
import Actions from 'main/components/Filters/Actions'
import './index.styl'

export default observer(function ProductList ({ style }) {
  return pug`
    View.root(style=style)
      Actions.found(
        found=24
        total=50
        sortBy='default'
      )
      View.products
        each product, index in products
          Product.product(
            key=index
            data=product
            styleName={
              first: index === 0,
              second: index === 1,
              third: index === 2,
              fourth: index === 3,
              even: index % 2 === 1,
              everyFourth: index % 4 === 0
            }
          )
  `
})
