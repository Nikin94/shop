import React from 'react'
import { View, Image, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Header ({ style }) {
  const path = ['home', 'shop']
  return pug`
    View.root(style=style)
      Image.image(
        source={uri: 'https://demo.hasthemes.com/neha-preview-tm/neha/assets/img/bg/breadcrumb.jpg'}
        resizeMode='contain'
      )
      View.text
        Text.main Shop
        View.breadcrumbs
          each breadcrumb, index in path
            Text.path(
              key=index 
              styleName={
                first: index === 0,
                last: index === path.length - 1
              }
            )= breadcrumb
              unless index === path.length - 1
                Text.path(styleName=['slash']) /
  `
})
