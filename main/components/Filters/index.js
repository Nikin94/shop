import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'

import Filter from './Filter'
import Actions from './Actions'

import Search from './Search'
import Price from './Price'
import Categories from './Categories'
import Color from './Color'
import Size from './Size'
import TopRatedProducts from './TopRatedProducts'

import './index.styl'

export default observer(function Filters ({ style }) {
  const components = [
    {
      value: Search,
      title: 'Search Products'
    },
    {
      value: Price,
      title: 'Filter By Price'
    },
    {
      value: Categories,
      title: 'Categories'
    },
    {
      value: Color,
      title: 'color'
    },
    {
      value: Size,
      title: 'size'
    },
    {
      value: TopRatedProducts,
      title: 'Top rated products'
    }
  ]

  const getComponent = item => {
    const Component = item
    return pug`Component`
  }

  return pug`
    View.root(style=style)
      each item, index in components
        Filter.filter(key=index title=item.title styleName={first: index === 0})
          =getComponent(item.value)
      Actions.found(
        found=24
        total=50
        sortBy='default'
      )
  `
})
