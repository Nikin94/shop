import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Color ({ style }) {
  const colors = ['#ff4136', '#ff01f0', '#3649ff', '#00c0ff', '#00ffae', '#8a00ff']
  return pug`
    View.root(style=style)
      each color, index in colors
        View.circleWrapper(
          key=index
          styleName={first: index === 0}
        )
          View.circle(style={backgroundColor: color})
  `
})
