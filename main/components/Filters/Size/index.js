import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Size ({ style }) {
  const sizes = ['XL', 'M', 'L', 'ML', 'LM']
  return pug`
    View.root(style=style)
      each size, index in sizes
        Text.size(
          key=index
          styleName={first: index === 0}
        )= size
  `
})
