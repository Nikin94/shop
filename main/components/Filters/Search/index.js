import React from 'react'
import { View, TextInput, TouchableOpacity } from 'react-native'
import { observer } from 'startupjs'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import './index.styl'

export default observer(function Search ({ style }) {
  const search = () => {}

  return pug`
    View.root(style=style)
      TextInput.input(placeholder='Search Products...')
      TouchableOpacity.search(onPress=search)
        FontAwesomeIcon(
          icon=faSearch
          color='#ff4136'
          size=18
        )
  `
})
