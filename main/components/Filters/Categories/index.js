import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Categories ({ style }) {
  const categories = [
    {
      label: 'Accessories',
      value: 4
    },
    {
      label: 'Book',
      value: 9
    },
    {
      label: 'Clothing',
      value: 5
    },
    {
      label: 'Homelife',
      value: 3
    },
    {
      label: 'Kids & Baby',
      value: 4
    }
  ]
  return pug`
    View.root(style=style)
      each category, index in categories
        View.category(key=index styleName={first: index === 0})
          Text.label= category.label
          Text.value= category.value
  `
})
