import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Price ({ style }) {
  return pug`
    View.root(style=style)
      View.range
        View.edge
        View.line
        View.edge
      View.price
        Text.text Price : $20 - $100
        Text.text Filter
  `
})
