import React from 'react'
import { View, Text, Image } from 'react-native'
import { observer } from 'startupjs'
import { Rating } from 'components'
import products from './products.json'
import './index.styl'

export default observer(function TopRatedProducts ({ style }) {
  return pug`
    View.root(style=style)
      each product, index in products
        View.product(
          key=index
          styleName={first: index === 0}
        )
          Image.image(source={uri: product.url})
          View.info
            Text.title= product.title
            Rating.rating(value=product.rating)
            Text.price= product.currency + product.price + '.00' 
  `
})
