import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { observer } from 'startupjs'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChevronDown, faTh, faBars } from '@fortawesome/free-solid-svg-icons'
import './index.styl'

export default observer(function Actions ({
  style,
  found,
  total,
  sortBy
}) {
  return pug`
    View.root(style=style)
      Text.text(styleName=['number'])= found + ' '
        Text.text Products found of
        Text.text(styleName=['number'])= ' ' + total
      View.row
        Text.sortBy Sort By :
        TouchableOpacity.sort
          Text.sortBy(styleName=['value'])= sortBy
          FontAwesomeIcon(
            icon=faChevronDown
            color='#626262'
            size=18
          )
      View.row
        FontAwesomeIcon(
          icon=faTh
          color='#ff4136'
          size=24
        )
        FontAwesomeIcon.bars(
          icon=faBars
          color='#6c6c6c'
          size=24
        )

  `
})
