import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Filter ({ style, title, children }) {
  return pug`
    View.root(style=style)
      Text.title= title
      View.content
        =children
  `
})
