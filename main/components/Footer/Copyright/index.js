import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Copyright ({ style }) {
  return pug`
    View.root(style=style)
      Text.text Copyright © neha 2020 . All Right Reserved.
  `
})
