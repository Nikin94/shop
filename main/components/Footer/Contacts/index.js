import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import contacts from './contacts.json'
import './index.styl'

export default observer(function Contacts ({ style }) {
  return pug`
    View.root(style=style)
      each contact, index in contacts
        View.contact(key=index styleName={first: index === 0})
          Text.label= contact.label
          Text.label= ' : '
          Text.value= contact.value
  `
})
