import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import { Logo, Menu } from 'components'
import Socials from './Socials'
import Copyright from './Copyright'
import Contacts from './Contacts'
import './index.styl'

export default observer(function Footer ({ style }) {
  return pug`
    View.root(style=style)
      View.container
        View.row
          Socials
          View.column
            Logo.logo
            Menu
            View.line
            Copyright
          Contacts.contacts(styleName=['desktop'])
        Contacts.contacts(styleName=['mobile'])
  `
})
