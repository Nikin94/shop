import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import { faTwitter, faTumblr, faFacebook, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import './index.styl'

export default observer(function Socials ({ style }) {
  const icons = [faTwitter, faTumblr, faFacebook, faInstagram]
  return pug`
    View.root(style=style)
      Text.exp 20 Years Experience
      View.icons
        each icon, index in icons
          View.icon(key=index styleName={first: index === 0})
            FontAwesomeIcon(
              icon=icon
              size=14
              color='#585858'
            )

  `
})
