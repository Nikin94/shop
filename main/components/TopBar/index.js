import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import { Logo, Menu } from 'components'
import Actions from './Actions'
import './index.styl'

export default observer(function TopBar ({ style }) {
  return pug`
    View.root(style=style)
      Logo
      Menu.menu(additionalItems=['contact'])
      Actions
  `
})
