import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import { faSearch, faShoppingBag, faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import './index.styl'

export default observer(function Actions ({ style }) {
  const actions = [faSearch, faShoppingBag, faBars]
  return pug`
    View.root(style=style)
      each action, index in actions
        View.icon(
          key=index
          styleName={
            first: index === 0,
            last: index === actions.length - 1
          }
        )
          FontAwesomeIcon(
            icon=action
            size=18
          )
  `
})
