import React from 'react'
import { ScrollView, View } from 'react-native'
import { observer } from 'startupjs'
import { Header, TopBar, Filters, ProductList, Footer } from 'main/components'
import './index.styl'

export default observer(function PHome () {
  return pug`
    ScrollView.root
      View.content
        TopBar
        Header
        View.wrapper
          Filters
          ProductList
        Footer
  `
})
