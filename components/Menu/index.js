import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Menu ({ style, additionalItems = [] }) {
  const menuItems = ['Home', 'About us', 'Shop', 'Blog', 'Pages']
  return pug`
    View.root(style=style)
      each item, index in menuItems.concat(additionalItems)
        Text.item(
          key=index
          styleName={first: index === 0}
        )= item
  `
})
