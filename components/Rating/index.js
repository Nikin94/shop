import React from 'react'
import { View } from 'react-native'
import { observer } from 'startupjs'
import { faStar } from '@fortawesome/free-regular-svg-icons'
import { faStar as faStarSolid } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import './index.styl'

export default observer(function Rating ({ style, value }) {
  let stars = 5
  return pug`
    View.root(style=style)
      while stars > 0
        - const active = stars + value > 5
        View.star(styleName={first: stars === 5} key=stars)
          FontAwesomeIcon(
            style={color: active ? '#ee3333' : '#000'}
            icon=active ? faStarSolid : faStar
            size=12
          )
          - stars--
  `
})
