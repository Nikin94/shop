import React from 'react'
import { Image, View } from 'react-native'
import { observer } from 'startupjs'
import './index.styl'

export default observer(function Logo ({ style }) {
  return pug`
    View.root(style=style)
      Image(
        style={width: 116, height: 31}
        source={uri: 'https://demo.hasthemes.com/neha-preview-tm/neha/assets/img/logo/logo.png'}
      )
  `
})
